﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeDSTwoWeekStudyPlanSolution
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution217 obj = new Solution217();
            int[] nums = { 1, 2, 3, 1 };
            var watch = System.Diagnostics.Stopwatch.StartNew();
            bool status = obj.ContainsDuplicate(nums);
            watch.Stop();
            Console.WriteLine(status);
            var elapsedMs = watch.Elapsed.TotalSeconds;
            Console.WriteLine("Total time taken: {0}", elapsedMs);


            watch = System.Diagnostics.Stopwatch.StartNew();
            status = obj.ContainsDuplicate2(nums);
            watch.Stop();
            Console.WriteLine(status);
            elapsedMs = watch.Elapsed.TotalSeconds;
            Console.WriteLine("Total time taken: {0}", elapsedMs);

            watch = System.Diagnostics.Stopwatch.StartNew();
            status = obj.ContainsDuplicate3(nums);
            watch.Stop();
            Console.WriteLine(status);
            elapsedMs = watch.Elapsed.TotalSeconds;
            Console.WriteLine("Total time taken: {0}", elapsedMs);


            Console.ReadLine();
        }
    }
}
