﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeDSTwoWeekStudyPlanSolution
{
    class Solution217
    {
        /// <summary>
        /// My Solution contains a simple linear search algorithm using 2 loops.
        /// On LeetCode it took 2860 ms
        /// </summary>
        /// <param name="nums"></param>
        /// <returns></returns>
        public bool ContainsDuplicate(int[] nums)
        {            
            bool status = false;
            if (nums.Length == 0)
            {
                return false;
            }

            for(int i = 0;i<nums.Length;i++)
            {
                int n = nums[i];
                for(int j = 0;j<nums.Length;j++)
                {
                    if(j==i)
                    {
                        continue;
                    }
                    if(n==nums[j])
                    {
                        status = true;
                        break;
                    }
                }

                if(status)
                {
                    break;
                }
            }            
            return status;
        }

        /// <summary>
        /// Better solutio found in LeetCode discussion section
        /// This uses Hashtable. Need to understand these data structures well.
        /// </summary>
        /// <param name="nums"></param>
        /// <returns></returns>
        public bool ContainsDuplicate2(int[] nums)
        {
            Hashtable hash = new Hashtable();

            foreach(var item in nums)
            {
                if(hash.Contains(item))
                {
                    return true;
                }
                else
                {
                    hash.Add(item, null);
                }
            }
            return false;
        }

        public bool ContainsDuplicate3(int[] nums)
        {
            return new HashSet<int>(nums).Count < nums.Length;
        }
    }
}
